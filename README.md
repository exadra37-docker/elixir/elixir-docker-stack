# ELIXIR DOCKER IMAGE


## MENU

* **The Package**
    + [Why Exists?](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/the-package/why_exists.md)
    + [What Is It?](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/the-package/what_is_it.md)
    + [When To use It?](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/the-package/when_to_use_it.md)
* **How To**
    + [Install](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/how-to/install.md)
    + [Use](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/how-to/use.md)
    + [Report an Issue](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/how-to/create_an_issue.md)
    + [Create a Branch](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/how-to/create_branches.md)
    + [Open a Merge Request](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/how-to/create_a_merge_request.md)
    + [Uninstall](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/how-to/uninstall.md)
* **Demos**
    + [Elixir - Hello World](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/docs/demos/elixir-hello-world.md)
* **Road Map**
    + [Milestones](https://gitlab.com/exadra37-docker/elixir/elixir/milestones)
    + [Overview](https://gitlab.com/exadra37-docker/elixir/elixir/boards)
* **About**
    + [Author](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/AUTHOR.md)
    + [Contributors](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/CONTRIBUTORS.md)
    + [Contributing](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/CONTRIBUTING.md)
    + [License](https://gitlab.com/exadra37-docker/elixir/elixir/blob/master/LICENSE)


## SUPPORT DEVELOPMENT

If this is useful for you, please:

* Share it on [Twitter](https://twitter.com/home?status=Base%20%23DockerImage%20for%20%23Elixir%20%23developers%20https%3A//gitlab.com/exadra37-docker/elixir/elixir%20by%20%40Exadra37.%20%23docker%20%23dockercontainers%20%23myelixirstatus) or in any other channel of your preference.
* Consider to [offer me](https://www.paypal.me/exadra37) a coffee, a beer, a dinner or any other treat 😎.


## EXPLICIT VERSIONING

This repository uses [Explicit Versioning](https://gitlab.com/exadra37-versioning/explicit-versioning) schema.


## BRANCHES

Branches are created as demonstrated [here](docs/how-to/create_branches.md).

This are the type of branches we can see at any moment in the repository:

* `master` - issues and milestones branches will be merged here. Don't use it in
              production.
* `last-stable-release` - matches the last stable tag created. Useful for
                           automation tools. Doesn't guarantee backwards
                           compatibility.
* `4-fix-some-bug` - each issue will have is own branch for development.
* `milestone-12_add-some-new-feature` - all Milestone issues will start, tracked and merged
                             here.

Only `master` and `last-stable-release` branches will be permanent ones in the
repository and all other ones will be removed once they are merged.


## DISCLAIMER

I code for passion and when coding I like to do as it pleases me...

You know I do this in my free time, thus I want to have fun and enjoy it ;).

Professionally I will do it as per company guidelines and standards.
